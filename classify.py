import pandas as pd
import pickle
import pathlib

#%% Loading the project and the model
fluctuations = pd.read_csv("data/absolute-commit-fluctuations/qualire.csv")

model_file = open("models/userstudy", "rb")
model = pickle.load(model_file)
model_file.close()

#%% Exploring the project's data
print(fluctuations.columns)
print(fluctuations.shape)

#%% Predicting the results
X = fluctuations.iloc[:, -52:] # select the 52 metrics
predicted = model.predict(X)
predicted_probability = model.predict_proba(X)

#%% Showing the results
fluctuations['SOLID'] = predicted
result = fluctuations[['revision', 'class_affected', 'metrics_changed', 'SOLID']]

#%% Saving the results
solid = result.loc[result["SOLID"] == 1].sample(10)
no_solid = result.loc[result["SOLID"] == 0].sample(10)

sample = solid.append(no_solid, ignore_index=True)

pathlib.Path('./predictions/').mkdir(parents=True, exist_ok=True)
sample.to_csv("./predictions/qualire-sample.csv", index=False)
