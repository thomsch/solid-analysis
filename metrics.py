#%%
import pandas as pd

metrics = pd.read_csv("data/metrics.csv")

#%%
metrics.head()
metrics.groupby('metric').count().sort_values(by="project", ascending=False)

#%%
occurrences = pd.read_csv("data/metrics-occurrences.csv")
occurrences.head()
occurrences[['count', 'category']].groupby('category').count().sort_values(by="count", ascending=False)
