# Import the necessary libraries
import matplotlib.pyplot as plt
import seaborn as sns
import oracle as o
import pandas as pd
import pathlib

import importlib
importlib.reload(o)

pathlib.Path('./figures/oracle').mkdir(parents=True, exist_ok=True)

oracle = o.load()
print("Number of projects: {}".format(len(oracle.Project.unique())))
counts = oracle.SOLID.value_counts()
print("Number of SOLID: {}".format(counts[1]))
print("Number of NOT SOLID: {}".format(counts[0]))
print("Ratio: 1:{:.2f} SOLID:NOT SOLID".format(counts[0] / counts[1]))

#%% Count for each flag
oracle.groupby("Project").sum()

#%% How many instances of SRP, OCP, LSP, ISP, DIP ?
oracle.groupby("Project").sum().sum()

#%%
sns.countplot(x="SOLID", data=oracle)
plt.savefig("./figures/oracle/solid-count.pdf", format="pdf")
plt.show()

#%%
a = oracle.groupby("Project")["SOLID"].value_counts()
a = pd.DataFrame(a)
a.columns = ["value"]
a = a.reset_index()
sns.catplot(x="SOLID", kind="box", y="value", data=a)
plt.savefig("./figures/oracle/solid-boxplot.pdf", format="pdf")
plt.show()

#%%
ax = sns.countplot(x="Project", data=oracle, hue="SOLID")
ax.set_xticklabels(ax.get_xticklabels(), rotation=40, ha="right")
plt.tight_layout()
plt.savefig("./figures/oracle/solid-count-per-project.pdf", format="pdf")
plt.show()