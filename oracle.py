import os
import pandas as pd


def files():
    return [file for file in os.listdir(path()) if os.path.isfile(os.path.join(path(), file))]


def load():
    solid_column_name = "SOLID"
    project_column_name = "Project"

    result = pd.DataFrame()
    for file in files():
        if("ignored" in file):
            continue
        oracle = pd.read_csv(os.path.join(path(), file))
        oracle[project_column_name] = os.path.splitext(file)[0]
        result = result.append(oracle, ignore_index=True)

    result[solid_column_name] = result[['SRP', 'OCP', 'LSP', 'ISP', 'DIP']].max(axis=1)

    result.SRP = result.SRP.astype('bool')
    result.OCP = result.OCP.astype('bool')
    result.LSP = result.LSP.astype('bool')
    result.ISP = result.ISP.astype('bool')
    result.DIP = result.DIP.astype('bool')
    result.SOLID = result.SOLID.astype('bool')

    return result[[project_column_name, 'Commit', 'Notes', 'SRP', 'OCP', 'LSP', 'ISP', 'DIP', solid_column_name, 'Tangled']]


def path():
    return "./data/oracle"
