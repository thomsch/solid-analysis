"""Generate the underlying decision tree of a model"""

import pickle

import graphviz
from sklearn import tree

with open("models/userstudy", "rb") as model_file:
    model = pickle.load(model_file)

column_names = ['LCOM5', 'NL', 'NLE', 'WMC', 'CBO', 'CBOI', 'NII', 'NOI', 'RFC', 'AD', 'CD', 'CLOC', 'DLOC', 'PDA', 'PUA', 'TCD', 'TCLOC', 'DIT', 'NOA', 'NOC', 'NOD', 'NOP', 'LLOC', 'LOC', 'NA', 'NG', 'NLA', 'NLG', 'NLM', 'NLPA', 'NLPM', 'NLS', 'NM', 'NOS', 'NPA', 'NPM', 'NS', 'TLLOC', 'TLOC', 'TNA', 'TNG', 'TNLA', 'TNLG', 'TNLM', 'TNLPA', 'TNLPM', 'TNLS', 'TNM', 'TNOS', 'TNPA', 'TNPM', 'TNS']

dot_data = tree.export_graphviz(model,
                                out_file=None,
                                feature_names=column_names,
                                class_names=['NO SOLID', 'SOLID'],
                                filled=True,
                                rounded=True,
                                proportion=True,
                                leaves_parallel=True)
graph = graphviz.Source(dot_data)
graph.render("figures/model/tree-study", view=False)