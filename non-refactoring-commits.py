#%%
import pandas as pd
from pathlib import Path

#%%
commits_path = "data/all-commits/hibernate-orm.txt"
refactorings_commits_path = "data/refactoring-commits/hibernate-orm.csv"
output_file = 'hibernate-orm.txt'
output_dir = Path('data/non-refactoring-commits')

#%%
commits = pd.read_csv(commits_path, header=None, names=['commit'])
refactoring_commits = pd.read_csv(refactorings_commits_path).drop(columns=['Date'])

result = set(commits['commit']) - set(refactoring_commits['CommitId'])

print("All size: {}".format(commits.shape[0]))
print("Refactoring size: {}".format(refactoring_commits.shape[0]))
print("Non refactoring commits: {}".format(len(result)))

len(set(result) & set(refactoring_commits['CommitId']))

#%%
output_dir.mkdir(parents=True, exist_ok=True)
pd.DataFrame(result, columns=['Commit']).to_csv(output_dir / output_file, index=False)
