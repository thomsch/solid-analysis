import pandas as pd
import numpy as np
import os
import graphviz
import oracle as o
from sklearn import tree
from sklearn import metrics
from sklearn.utils import class_weight
from sklearn.model_selection import RandomizedSearchCV
from scipy.stats import randint as sp_randint


def project_name(file):
    return os.path.splitext(file)[0]


def merge_project_data(file, fluctuation_file, oracle_file):
    fluctuations = pd.read_csv(os.path.join(fluctuation_file, file))
    features_columns = fluctuations.columns.tolist()[-52:] # Retrieve the columns names containing fluctuations.
    oracle = pd.read_csv(os.path.join(oracle_file, file))
    print("{} fluctuations {}".format(file, fluctuations.shape))
    print("{} oracle {}".format(file, oracle.shape))
    target_name = 'SOLID'
    oracle[target_name] = oracle[['SRP', 'OCP', 'LSP', 'ISP', 'DIP']].max(axis=1)

    dataset = oracle.merge(fluctuations, left_on="Commit", right_on="revision")[['Commit'] + features_columns + [target_name]]
    dataset['project'] = project_name(file)

    if len(dataset) != len(oracle):
        print("WARNING: The number of observations in the dataset {} is different from the oracle's {}.".format(len(dataset), len(oracle)))

    return dataset


def split_feature_and_target(data):
    x = data.drop(columns=['project', 'Commit', 'SOLID'])
    y = data[['SOLID']].astype('int')
    return x, y


def report_models(results, n_top=3):
    for i in range(1, n_top + 1):
        candidates = np.flatnonzero(results['rank_test_score'] == i)
        for candidate in candidates:
            print("Model with rank: {0}".format(i))
            print("Mean validation score: {0:.3f} (std: {1:.3f})".format(
                  results['mean_test_score'][candidate],
                  results['std_test_score'][candidate]))
            print("Parameters: {0}".format(results['params'][candidate]))
            print("")

#%% Finding projects
oraclesPath = o.path()
fluctuationPath = "./data/absolute-commit-fluctuations"
extension = ".csv"

# Loading data in folders data/oracle and data/rev-fluctuation. Each project is matched by the file names so make sure
# they are the same.
fluctuations = [file for file in os.listdir(fluctuationPath) if os.path.isfile(os.path.join(fluctuationPath, file))]
oracles = o.files()

projects = set(oracles) & set(fluctuations)
missing = set(oracles) ^ set(fluctuations)

print('Detected {} valid projects over {} found.'.format(len(projects), len(projects) + len(missing)))
print(projects)
print('The following projects will be ignored: {}'.format(missing))

#%% Compilation of the datasets
data_per_project = [merge_project_data(file, fluctuationPath, oraclesPath) for file in projects]
data = pd.DataFrame(columns=data_per_project[0].columns)

for project in data_per_project:
    data = data.append(project, ignore_index=True)

print(data.shape)

#%% Training and Testing
names = list(map(lambda file: project_name(file), projects))

results = pd.DataFrame()

i = 0
for name in names:
    i += 1

    # Separating training and testing projects
    test_project = name
    train = data[data.project != test_project]
    test = data[data.project == test_project]

    print("===== Exp. {}/{} =========================================".format(i, len(names)))
    print("> Training: {} - shape{}".format(train.project.unique(), train.shape))
    print("  NOSOLID: {}".format(train['SOLID'].value_counts()[0]))
    print("  SOLID  : {}".format(train['SOLID'].value_counts()[1]))
    print("> Testing: {} - shape{}".format(test.project.unique(), test.shape))
    print("  NOSOLID: {}".format(test['SOLID'].value_counts()[0]))
    print("  SOLID  : {}".format(test['SOLID'].value_counts()[1]))

    train_X, train_Y = split_feature_and_target(train)
    test_X, test_Y = split_feature_and_target(test)

    # Creating the model
    clf = tree.DecisionTreeClassifier(random_state=42)
    weights = class_weight.compute_sample_weight('balanced', y=train_Y)
    param_dist = {"max_depth": [None, 3, 5, 7, 8, 9, 10],
                  "max_features": sp_randint(1, 11),
                  "min_samples_split": sp_randint(2, 11),
                  "min_samples_leaf": sp_randint(1, 5),
                  "criterion": ["gini", "entropy"]}

    # Hyper parameter tuning
    random_search = RandomizedSearchCV(clf, param_distributions=param_dist,
                                       n_iter=100, cv=5, iid=False, scoring="f1")
    random_search.fit(train_X, train_Y, sample_weight=weights)

    # Report results of hyper parameter tuning
    report_models(random_search.cv_results_)

    # Testing the model on a new project
    clf = random_search.best_estimator_
    predicted = clf.predict(test_X)
    predicted_probability = clf.predict_proba(test_X)

    # Performance of the tested model
    report = metrics.classification_report(test_Y, predicted, target_names=['NO SOLID', 'SOLID'], output_dict=True)
    print(report)

    aurroc = metrics.roc_auc_score(test_Y, pd.DataFrame(predicted_probability).iloc[:, 1])
    print("Area Under ROC Curve: {}".format(aurroc))

    accuracy = metrics.balanced_accuracy_score(test_Y, predicted)
    print("Balanced accuracy: {}".format(accuracy))

    result = {'train': train.project.unique(),
              'test': test.project.unique(),
              'aurocc': aurroc,
              'f1': report['weighted avg']['f1-score'],
              'f1-solid': report['SOLID']['f1-score'],
              'f1-nosolid': report['NO SOLID']['f1-score'],
              'accuracy': accuracy
              }
    results = results.append(result, ignore_index=True)

    dot_data = tree.export_graphviz(clf,
                                    out_file=None,
                                    feature_names=train_X.columns,
                                    class_names=['NO SOLID', 'SOLID'],
                                    filled=True,
                                    rounded=True,
                                    proportion=True,
                                    leaves_parallel=True)
    graph = graphviz.Source(dot_data)
    graph.render("figures/model/tree-" + test_project, view=False)

print("===== Overall results ====================================")
print(results)
print("Mean Area Under ROC Curve: {:.3f} (std: {:.2f})".format(results['aurocc'].mean(), results['aurocc'].std()))
print("Mean accuracy: {:.3f} (std: {:.2f})".format(results['accuracy'].mean(), results['accuracy'].std()))
print("Mean F1 score: {:.3f} (std: {:.2f})".format(results['f1'].mean(), results['f1'].std()))
print("SOLID F1 score: {:.3f} (std: {:.2f})".format(results['f1-solid'].mean(), results['f1-solid'].std()))
print("NO SOLID F1 score: {:.3f} (std: {:.2f})".format(results['f1-nosolid'].mean(), results['f1-nosolid'].std()))

#%% Export of the decision tree.
# Graphviz library installation folder MUST be in your PATH or reachable from you python installation.
# You can use os.environ["PATH"] += os.pathsep + 'path/to/graphviz' as a temporary work around.

# dot_data = tree.export_graphviz(clf,
#                                 out_file="figures/model/tree-" + test_project,
#                                 feature_names=train_X.columns,
#                                 class_names=['NO SOLID', 'SOLID'],
#                                 filled=True,
#                                 rounded=True,
#                                 proportion=True,
#                                 leaves_parallel=True)
# graph = graphviz.Source(dot_data)
# graph.view()
# graph.render("tree")
